#!/usr/bin/env python3.7
#
# Concurrent publishers
#
# SEE https://www.roguelynn.com/words/asyncio-true-concurrency/
#

import asyncio
import logging
import random
import string
from uuid import uuid4

import attr

logging.basicConfig(
  level=logging.INFO,
  format="%(asctime)s,%(msecs)d: %(message)s",
  datefmt='%H:%M:%S'
  )

log = logging.getLogger(__name__)



def _uuid():
  return str(uuid4())

@attr.s(auto_attribs=True)
class Message:
  publisher_id: str=attr.ib(repr=False)
  message_id: str=attr.ib(factory=_uuid)


# emulates an external pub/sub-like system
async def publish(queue: asyncio.Queue, publisher_id):
  count = 0
  while True:
    # create message
    msg = Message(publisher_id)

    # publish
    # put the message on the queue as soon as it gets a chance
    asyncio.create_task( queue.put(msg) )
    count += 1
    log.info(f"{publisher_id:6} published its {count:2d}th msg: {msg}")

    # Emulates some i/o operation. e.g. when publisher decides to create message
    await asyncio.sleep(random.random())


def main_36():
  queue = asyncio.Queue()
  loop = asyncio.get_event_loop()
  try:
    for publisher_id in ('pedro', 'manuel', 'odei'):
      loop.create_task(publish(queue, publisher_id))
    loop.run_forever()
  except KeyboardInterrupt:
    log.info("Interrupted")
    # TODO: close pending tasks!!
  finally:
    loop.close()


if __name__ == "__main__":
  main_36()
