export APPNAME=pubs4_app

.venv:
	python3 -m venv $@
	$@/bin/pip3 install -U pip setuptools wheel

.PHONY: devenv
devenv: .venv requirements.txt ## Development environment
	# installing tooling
	@$</bin/pip3 install autopep8 pylint
	# installing requirements
	@$</bin/pip3 install -r requirements.txt

.PHONY: clean
clean:
	@git clean -dxf -e .vscode
	# stopping ${APPNAME}
	-@docker rm ${APPNAME}
	# removing image
	-@docker image rm -f pubsub-pattern_app:latest



# DOCKER app
build:
	docker-compose build app

up:
	docker-compose up

down:
	docker-compose down

shell:
	docker exec -it ${APPNAME} /bin/sh
	# ps ax
	# WARNING: pkill has different options in osx and alpine
	# pkill -TERM ""...??