#!/usr/bin/env python3.7
#
# Exception Handling with asyncio
#
# SEE https://www.roguelynn.com/words/asyncio-exception-handling/
#

import asyncio
import functools
import logging
import random
import signal
import string
import time
from datetime import datetime, timedelta
from enum import IntEnum
# Main program
from typing import Dict, List, Optional
from uuid import uuid4

import attr

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s,%(msecs)d: %(message)s",
    datefmt='%H:%M:%S'
)

log = logging.getLogger(__name__)


def _uuid():
    # emulates id assigned by consumer
    return str(uuid4()).split('-')[0]


@attr.s(auto_attribs=True)
class Message:
    pub_id: int = attr.ib(repr=False)
    msg_id: str = attr.ib(factory=_uuid)
    hostname: str = attr.ib(repr=False, init=False)
    is_restarted: bool = attr.ib(repr=False, default=False)
    is_saved: bool = attr.ib(repr=False, default=False)
    is_acked: bool = attr.ib(repr=False, default=False)
    extension_count: int = attr.ib(repr=False, default=0)

    def __attrs_post_init__(self):
        self.hostname = f"{self.msg_id}.example.net"


# Producer
# emulates an external pub/sub-like system
async def publish(queue: asyncio.Queue):
    while True:
        # publisher message
        msg = Message(pub_id=random.randint(1, 6))

        # publish
        # put the message on the queue as soon as it gets a chance
        asyncio.create_task(queue.put(msg))
        log.info(f"Published {msg} by [{msg.pub_id}]")

        # Emulates some i/o operation. e.g. when publisher decides to create message
        await asyncio.sleep(random.random()*5)


# Consumer operations

async def consume(queue: asyncio.Queue):
    while True:
        msg = await queue.get()
        if msg is None:
            break

        asyncio.create_task(handle_message(msg))

    log.info("Stopping consuming")


def handle_errors(results: List, msg: Message):
    for res in results:
        if isinstance(res, RestartFailed):
            log.info(f"retry restarting {msg.hostname}??")
        elif isinstance(res, Exception):
            log.info(f"error while handling {msg}")


async def handle_message(msg: Message):
    done = asyncio.Event()

    asyncio.create_task(cleanup_or_extend(msg, done))

    results = await asyncio.gather(
        restart_host(msg),
        save(msg),
        return_exceptions=True
    )
    # exceptions are treated the same as successful results, and aggregated in the result list.
    handle_errors(results, msg) # does NOT throw

    # TODO: add retry if failure. use tenacity for it

    # this will never be hit if any of the tasks above raise
    done.set()


class RestartFailed(Exception):
    pass

class SaveFailed(Exception):
    pass

async def restart_host(msg: Message):
    # emulates i/o operations, e.g. calling another service API
    await asyncio.sleep(random.random())

    # emulates failure
    if random.randint(1,5) == 3:
        raise RestartFailed(f"Cannot restart {msg.hostname}")

    msg.is_restarted = True
    log.info(f"Restarted {msg.hostname}")


async def save(msg: Message):
    # emulates e.g. access to db service
    await asyncio.sleep(random.random())

    # emulates failure
    if random.randint(1, 5) == 3:
        raise SaveFailed(f"Cannot save {msg}")

    msg.is_saved = True
    log.info(f"Saved {msg}")


async def cleanup_async(msg: Message):
    # We need to acknowledge that we’re done with the message
    #  so it isn’t re-delivered by mistake

    # emulates ACK message to publisher
    await asyncio.sleep(random.random())

    msg.is_acked = True

    extra = ""
    if not msg.is_restarted:
        extra += "FAILED to restart "
    if not msg.is_saved:
        extra += "FAILED to save "
    log.info(f"Done {msg} and acknowledged to [{msg.pub_id}]. {extra}")


async def cleanup_or_extend(msg: Message, done: asyncio.Event):
    # Asks for extension if
    MAX_ACK_TIMEOUT_SECS = 3
    while not done.is_set():
        msg.extension_count += 1
        log.info(
            f"Extended {msg} by {MAX_ACK_TIMEOUT_SECS} secs [delayed {msg.extension_count*MAX_ACK_TIMEOUT_SECS}]")
        await asyncio.sleep(0.8 * MAX_ACK_TIMEOUT_SECS)
    else:
        await cleanup_async(msg)


def handle_uncaught_exception(loop: asyncio.BaseEventLoop, context: Dict):
    # https://docs.python.org/3/library/asyncio-eventloop.html#asyncio.loop.call_exception_handler
    error = context.get("exception") or context['message']

    log.error(f"Caught exception '{error}'")
    log.warning("Shutting down")
    asyncio.create_task(shutdown(loop))


async def shutdown(loop: asyncio.BaseEventLoop, signum: Optional[IntEnum] = None):
    # graceful shutdown of all service subsystems
    if signum:
        log.info("Received signal %s", signum)

    log.info("Saving stuff before ending")
    log.info("Closing db connections")
    # stop unfinished tasks

    pending_tasks = [t for t in asyncio.all_tasks()
                     if t is not asyncio.current_task()]

    log.info(f"Canceling {len(pending_tasks)} pending tasks ...")
    for task in pending_tasks:
        task.cancel()

    await asyncio.gather(*pending_tasks, 
        return_exceptions=True # ignore errors while closing tasks
    )

    log.info("Flusing metrics here")

    loop.stop()


# Main program


def main_36(num_publishers=2):
    queue = asyncio.Queue()
    loop = asyncio.get_event_loop()

    # OS signals

    # There are many common signals that a service should expect and handled,e.g.
    # SIGHUP - Hangup detected on controlling terminal or death of controlling process
    # SIGQUIT - Quit from keyboard(via ^\)
    # SIGTERM - Termination signal
    # SIGINT - Interrupt program
    watch = (
        signal.SIGQUIT,
        signal.SIGTERM,
        signal.SIGINT
    )
    for sig in watch:
        # NOTE: see https://docs.python-guide.org/writing/gotchas/#late-binding-closures
        loop.add_signal_handler(
            sig, lambda sig=sig: asyncio.create_task(shutdown(loop, signum=sig))
        )

    # Exceptions
    loop.set_exception_handler(handle_uncaught_exception)

    try:
        loop.create_task(publish(queue))
        loop.create_task(consume(queue))
        loop.run_forever()
    finally:
        loop.close()
        log.info("Successfully shut-down")


if __name__ == "__main__":
    main_36()
