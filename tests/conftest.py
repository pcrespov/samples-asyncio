import pytest
from typing import Optional

@pytest.fixture
def create_coroutine_mock(mocker, monkeypatch):
    def _create_coro_mock(to_patch: Optional[str]=None):
        mock = mocker.Mock()

        # converts mock into a coroutine
        async def _coro(*arg, **kargs):
            return mock(*arg, **kargs)

        if to_patch:
            monkeypatch.setattr(to_patch, _coro)

        return mock, _coro

    return _create_coro_mock


@pytest.fixture
def mock_sleep(create_mock_coro):
    # won't need the returned coroutine here
    mock, _ = create_mock_coro(to_patch="mayhem.asyncio.sleep")
    return mock
