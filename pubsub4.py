#!/usr/bin/env python3.7
#
# Graceful shutdown
#
# SEE https://www.roguelynn.com/words/asyncio-graceful-shutdowns/
#

import signal
import asyncio
import functools
import logging
import random
import string
import time
from datetime import datetime, timedelta
from uuid import uuid4

import attr

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s,%(msecs)d: %(message)s",
    datefmt='%H:%M:%S'
)

log = logging.getLogger(__name__)


def _uuid():
    # emulates id assigned by consumer
    return str(uuid4()).split('-')[0]


@attr.s(auto_attribs=True)
class Message:
    pub_id: int = attr.ib(repr=False)
    msg_id: str = attr.ib(factory=_uuid)
    hostname: str = attr.ib(repr=False, init=False)
    is_restarted: bool = attr.ib(repr=False, default=False)
    is_saved: bool = attr.ib(repr=False, default=False)
    is_acked: bool = attr.ib(repr=False, default=False)
    extension_count: int = attr.ib(repr=False, default=0)

    def __attrs_post_init__(self):
        self.hostname = f"{self.msg_id}.example.net"


# Producer
# emulates an external pub/sub-like system
async def publish(queue: asyncio.Queue):
    while True:
        # publisher message
        msg = Message(pub_id=random.randint(1, 6))

        # publish
        # put the message on the queue as soon as it gets a chance
        asyncio.create_task(queue.put(msg))
        log.info(f"Published {msg} by [{msg.pub_id}]")

        # Emulates some i/o operation. e.g. when publisher decides to create message
        await asyncio.sleep(random.random()*10)


# Consumer operations

async def consume(queue: asyncio.Queue):
    while True:
        msg = await queue.get()
        if msg is None:
            break

        asyncio.create_task(handle_message(msg))

    log.info("Stopping consuming")


async def handle_message(msg: Message):
    done = asyncio.Event()
    asyncio.create_task(cleanup_or_extend(msg, done))

    await asyncio.gather(
        restart_host(msg),
        save(msg)
    )
    done.set()


async def restart_host(msg: Message):
    # emulates i/o operations, e.g. calling another service API
    await asyncio.sleep(random.random())
    msg.is_restarted = True
    log.info(f"Restarted {msg.hostname}")


async def save(msg: Message):
    # emulates e.g. access to db service
    await asyncio.sleep(random.random())
    msg.is_saved = True
    log.info(f"Saved {msg}")


async def cleanup_async(msg: Message):
    # We need to acknowledge that we’re done with the message
    #  so it isn’t re-delivered by mistake

    # emulates ACK message to publisher
    await asyncio.sleep(random.random())

    msg.is_acked = True
    log.info(f"Done {msg} and acked to [{msg.pub_id}]")


async def cleanup_or_extend(msg: Message, done: asyncio.Event):
    # Asks for extension if
    MAX_ACK_TIMEOUT_SECS = 3
    while not done.is_set():
        msg.extension_count += 1
        log.info(
            f"Extended {msg} by {MAX_ACK_TIMEOUT_SECS} secs [delayed {msg.extension_count*MAX_ACK_TIMEOUT_SECS}]")
        await asyncio.sleep(0.8 * MAX_ACK_TIMEOUT_SECS)
    else:
        await cleanup_async(msg)


async def shutdown(signum: int, loop: asyncio.BaseEventLoop):
    # graceful shutdown of all service subsystems
    log.info("Received signal %s", signum)
    log.info("Saving stuff before ending")
    # stop unfinished tasks

    pending_tasks = [t for t in asyncio.all_tasks()
                     if t is not asyncio.current_task()]

    log.info(f"Canceling {len(pending_tasks)} pending tasks ...")
    for task in pending_tasks:
        task.cancel()
    await asyncio.gather(*pending_tasks, return_exceptions=True)
    log.info("Flusing metrics here")
    loop.stop()


# Main program


def main_36(num_publishers=2):
    queue = asyncio.Queue()
    loop = asyncio.get_event_loop()

    # OS signals

    # There are many common signals that a service should expect and handled,e.g.
    # SIGHUP - Hangup detected on controlling terminal or death of controlling process
    # SIGQUIT - Quit from keyboard(via ^\)
    # SIGTERM - Termination signal
    # SIGINT - Interrupt program
    watch = (
        signal.SIGQUIT,
        signal.SIGTERM,
        signal.SIGINT
    )
    for sig in watch:
        # NOTE: see https://docs.python-guide.org/writing/gotchas/#late-binding-closures
        loop.add_signal_handler(
            sig, lambda sig=sig: asyncio.create_task(shutdown(sig, loop))
        )

    try:
        loop.create_task(publish(queue))
        loop.create_task(consume(queue))
        loop.run_forever()

    finally:
        loop.close()
import sys

if __name__ == "__main__":
    if len(sys.argv)>1:
        _, level = sys.argv[1].split("=")
        log.setLevel(getattr(logging, level.upper()))
    main_36()
