# sample-asyncio

Excercising asyncio ... 

- Excelent 7 parts post [asyncio: We Did It Wrong](https://www.roguelynn.com/words/asyncio-we-did-it-wrong/) by [Lynn root](https://www.roguelynn.com)
  - [code](https://github.com/econchick/mayhem)
  - [video](https://www.youtube.com/watch?v=1lJDZx6f6tY)
- [Why Your Dockerized Application Isn’t Receiving Signals](https://hynek.me/articles/docker-signals/) by [Hynek Schlawack](https://hynek.me) 
- python [common gotchas](https://docs.python-guide.org/writing/gotchas/)
  