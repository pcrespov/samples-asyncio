#!/usr/bin/env python3.7
#
# 
# SEE https://www.roguelynn.com/words/asyncio-initial-setup/
#

import asyncio
import logging
import random
import string
from uuid import uuid4

import attr

logging.basicConfig(
  level=logging.INFO,
  format="%(asctime)s,%(msecs)d: %(message)s",
  datefmt='%H:%M:%S'
  )

log = logging.getLogger(__name__)



@attr.s(auto_attribs=True)
class Message:
  publisher: str
  identifier: str


# emulates publishing events
async def publish(queue: asyncio.Queue):
  count = 0
  while True:
    # create message
    msg = Message(
        publisher="".join(random.choices(string.ascii_letters, k=5)),
        identifier=str(uuid4())
    )

    # Emulates some i/o operation
    await asyncio.sleep(random.random())

    # publish
    await queue.put(msg)
    count += 1
    log.info(f"Published {msg} [{count}]")
    
    if count == 5:
      break
  await queue.put(None)

async def consume(queue: asyncio.Queue):
  while True:

    # wait for something to be publised
    msg = await queue.get()
    if msg is None:
      break

    # process msg
    log.info(f"Consumed {msg}")

    # emulates some other i/o tasks
    await asyncio.sleep(random.random())


def main_36_a():
  queue = asyncio.Queue()
  loop = asyncio.get_event_loop()
  try:
    # publishing BLOCKS consuming
    # consuming does not happen until all publishing is done
    loop.run_until_complete(publish(queue))
    loop.run_until_complete(consume(queue))
  finally:
    loop.close()

def main_36_b():
  queue = asyncio.Queue()
  loop = asyncio.get_event_loop()
  try:
    # More concurrent than previous case
    # Can consume some of the published messages
    # 
    loop.create_task(publish(queue))
    loop.create_task(consume(queue))
    loop.run_forever()
  except KeyboardInterrupt:
    log.info("Interrupted")
  finally:
    loop.close()

def main_36_c():
  queue = asyncio.Queue()
  loop = asyncio.get_event_loop()

  try:
    # TODO: not sure the difference in terms of
    # concurrency with previous. The outputs seem 
    # less concurrent
    #
    asyncio.gather(
      publish(queue), 
      consume(queue),
      loop = loop
    )
    loop.run_forever()
  except KeyboardInterrupt:
    log.info("Interrupted")
  finally:
    loop.close()


def main():
  queue = asyncio.Queue()
  asyncio.run(publish(queue))
  asyncio.run(consume(queue))

if __name__ == "__main__":
  main()
  #main_36_a()
  #main_36_b()
  #main_36_c()
