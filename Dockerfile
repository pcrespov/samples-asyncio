FROM python:3.7-alpine

# https://hynek.me/articles/docker-signals/

WORKDIR /app

COPY bin ./bin
COPY pubsub4.py \
    requirements.txt \
    ./
RUN pip3 install -r requirements.txt

# ENTRYPOINT ["/tini", "-v", "--", "/app/bin/entrypoint.sh"]
# Base includes already tini when run is called with  --init
# https://github.com/krallin/tini#using-tini

ENTRYPOINT ["/app/bin/entrypoint.sh"]
CMD [ "pubsub4.py", "--loglevel=DEBUG"]