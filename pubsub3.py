#!/usr/bin/env python3.7
#
# Concurrent consumers
#
# SEE https://www.roguelynn.com/words/asyncio-true-concurrency/
#

import asyncio
import functools
import logging
import random
import string
import time
from datetime import datetime, timedelta
from uuid import uuid4

import attr

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s,%(msecs)d: %(message)s",
    datefmt='%H:%M:%S'
)

log = logging.getLogger(__name__)


def _uuid():
    return str(uuid4()).split('-')[0]


@attr.s(auto_attribs=True)
class Message:
    pub_id: int = attr.ib(repr=False)
    msg_id: str = attr.ib(factory=_uuid)
    hostname: str = attr.ib(repr=False, init=False)
    is_restarted: bool = attr.ib(repr=False, default=False)
    is_saved: bool = attr.ib(repr=False, default=False)
    is_acked: bool = attr.ib(repr=False, default=False)
    extension_count: int = attr.ib(repr=False, default=0)

    def __attrs_post_init__(self):
        self.hostname = f"{self.msg_id}.example.net"


## Producer 
# emulates an external pub/sub-like system
async def publish(queue: asyncio.Queue, publisher_id: str):
    count = 0
    while True:
        # create message
        msg = Message(publisher_id)

        # publish
        # put the message on the queue as soon as it gets a chance
        asyncio.create_task(queue.put(msg))
        count += 1
        log.debug(f"[{publisher_id:2d}] published {count:2d} msgs: {msg}")

        # Emulates some i/o operation. e.g. when publisher decides to create message
        await asyncio.sleep(random.random()*10)


## Consumer operations

async def consume(queue: asyncio.Queue):
    while True:
        msg = await queue.get()
        if msg is None:
            break

        asyncio.create_task(handle_message(msg))

    log.info("Stopping consuming")


async def handle_message(msg: Message):
    # order of these tasks is unimportant
    # asyncio.create_task(handle_restart(msg))
    # asyncio.create_task(save(msg))

    # OPTION 1
    # gather_future = asyncio.gather(
    #   handle_restart(msg),
    #   save(msg))
    # # calls cleanup_sync(msg, fut) when gathered tasks are done
    # callback = functools.partial(cleanup_sync, msg)
    # gather_future.add_done_callback(callback)

    # OPTION 2
    #await asyncio.gather(
    #    handle_restart(msg),
    #    save(msg)
    #)
    #await cleanup_async(msg)

    # OPTION 2 with extension
    done = asyncio.Event()
    asyncio.create_task(cleanup_or_extend(msg, done))

    await asyncio.gather(
        handle_restart(msg),
        save(msg)
    )
    done.set()


async def handle_restart(msg: Message, max_days=2):
    # filters out sequentialy
    last_restart = await last_restart_date(msg.hostname)
    diff = datetime.now() - last_restart
    if diff.days > max_days:
        await restart_host(msg)
    else:
        log.info(
            f"Restarted {msg.hostname} SKIPPED. Last was {diff.days} days ago.")


async def last_restart_date(hostname: str):
    # emulate query to hostname monitoring
    await asyncio.sleep(random.random())
    restart_date = datetime.now() - timedelta(days=random.randint(0, 5))
    return restart_date


async def restart_host(msg: Message):
    # emulates i/o operations, e.g. calling another service API
    await asyncio.sleep(random.random())
    msg.is_restarted = True
    log.info(f"Restarted {msg.hostname}")


async def save(msg: Message):
    # emulates e.g. access to db service
    await asyncio.sleep(random.random())
    msg.is_saved = True
    log.info(f"Saved {msg}")


def cleanup_sync(msg: Message, fut: asyncio.Future):
    msg.is_acked = True
    log.info(f"Done with {msg}")


async def cleanup_async(msg: Message):
    # We need to acknowledge that we’re done with the message
    #  so it isn’t re-delivered by mistake

    # emulates ACK message to publisher
    await asyncio.sleep(random.random())

    msg.is_acked = True
    log.info(f"Done {msg} and acked to [{msg.pub_id}]")


async def cleanup_or_extend(msg: Message, done: asyncio.Event):
    # Asks for extension if
    MAX_ACK_TIMEOUT_SECS = 3
    while not done.is_set():
        msg.extension_count +=1
        log.info(
            f"Extended {msg} by {MAX_ACK_TIMEOUT_SECS} secs [delayed {msg.extension_count*MAX_ACK_TIMEOUT_SECS}]")
        await asyncio.sleep(0.8* MAX_ACK_TIMEOUT_SECS)
    else:
        await cleanup_async(msg)

## Main functions

def main_36(num_publishers=2):
    queue = asyncio.Queue()
    loop = asyncio.get_event_loop()
    try:
        for publisher_id in range(num_publishers):
            loop.create_task(publish(queue, publisher_id))

        loop.create_task(consume(queue))
        loop.run_forever()
    except KeyboardInterrupt:
        log.info("Interrupted")
        # TODO: close pending tasks!!
    finally:
        loop.close()


if __name__ == "__main__":
    main_36()
