#!/bin/sh

exec python $@



## https://hynek.me/articles/docker-signals/
# *exec* python $@

#PID   USER     TIME  COMMAND
#    1 root      0:00 /sbin/docker-init -- /app/bin/entrypoint.sh pubsub4.py --l
#    6 root      0:00 python pubsub4.py --loglevel=DEBUG
#    7 root      0:00 ps ax


# python $@
# launches python in a subprocess ... and therefore it does not receive kill signal

# PID   USER     TIME  COMMAND
#     1 root      0:00 /sbin/docker-init -- /app/bin/entrypoint.sh pubsub4.py --l
#     6 root      0:00 {entrypoint.sh} /bin/sh /app/bin/entrypoint.sh pubsub4.py 
#     7 root      0:00 python pubsub4.py --loglevel=DEBUG
#    15 root      0:00 ps